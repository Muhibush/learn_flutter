import 'package:flutter/material.dart';
import 'package:swipe_card/SwipeAnimation/index.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Swipe card',
      theme: new ThemeData(
        primarySwatch: Colors.blue
      ),
      home: new CardDemo(),
    );
  }
}

