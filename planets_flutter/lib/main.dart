import 'package:flutter/material.dart';
import 'package:planets_flutter/ui/home/HomePage.dart';
import 'package:planets_flutter/ui/detail/detail_page.dart';

void main(){
  runApp(new MaterialApp(
    title: "Planets",
    home: new HomePage(),
  ));
}