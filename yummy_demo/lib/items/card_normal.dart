import 'package:flutter/material.dart';
import 'package:yummy_demo/model/movie.dart';
import 'package:yummy_demo/ui/detail/detail.dart';
import 'package:cached_network_image/cached_network_image.dart';

Widget cardNormal(
    BuildContext context,
    Movie data
    ){
  return GestureDetector(
    onTap: (){
      print("5555555555555555555555555 ${data.title}");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Detail(movie: data),
        ),
      );
    },
    child: Container(
      height: 200.0,
      width: 200.0,
      child: new Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        elevation: 1.5,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            new CachedNetworkImage(
              imageUrl: "http://image.tmdb.org/t/p/w300${data.backdropPath!=null?data.backdropPath:"/VuukZLgaCrho2Ar8Scl9HtV3yD.jpg"}",
              fit: BoxFit.fill,
              height: 130.0,
              width: 100.0,
            ),
            new Padding(
                padding: EdgeInsets.all(10.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(data.title, maxLines: 1,),
                  new Text(data.popularity.toString()),
                  new Text(data.overview,maxLines: 3,)
                ],
              ),
            )
          ],
        ),
      ),
    ),
  );
}