import 'package:flutter/material.dart';
import 'package:yummy_demo/model/movie.dart';
import 'package:video_player/video_player.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:yummy_demo/services/api.dart' as api;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';
import 'package:chewie/chewie.dart';
import 'package:yummy_demo/features/recipe_swiper.dart';


class Detail extends StatefulWidget {
  final Movie movie;

  const Detail({Key key, @required this.movie}) : super(key: key);

  @override
  DetailState createState() {
    return new DetailState(movie);
  }
}

class DetailState extends State<Detail> {
  Movie movie;
  double rating;
  VideoPlayerController _controller;

  DetailState(Movie movie){
    this.movie = movie;
  }

  @override
  void initState() {
    super.initState();
    rating =0.0;
    _controller = new VideoPlayerController.network(
        "https://github.com/flutter/assets-for-api-docs/blob/master/assets/videos/butterfly.mp4?raw=true"
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: new Color(0xfff8faf8),
        centerTitle: true,
        elevation: 1.0,
        title: new Text(movie.title,
          style: new TextStyle(fontFamily: "Pacifico"),
        ),
      ),
      body: new ListView(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          new Chewie(
            _controller,
            aspectRatio: 3 / 2,
            autoPlay: false,
            looping: false,
            autoInitialize: true,
          ),
          //title
          new Card(
              margin: EdgeInsets.all(0.0),
              elevation: 1.0,
              child: Padding(
                padding: EdgeInsets.all(14.0),
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(movie.title,
                      style: new TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.w800),
                    ),
                    new Padding(padding: EdgeInsets.all(5.0)),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new SmoothStarRating(
                          allowHalfRating: false,
                          starCount: 5,
                          size: 20.0,
                          color: Colors.yellow,
                          borderColor: Colors.yellow,
                          rating: 4.5,
                        ),
                        new Text("(${rating.toStringAsFixed(1)})")
                      ],
                    )
                  ],
                ),
              )
          ),
          //link
          new Card(
            margin: EdgeInsets.fromLTRB(0.0, 1.0, 0.0, 0.0),
            elevation: 1.0,
            child: new Padding(
              padding: EdgeInsets.all(14.0),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text("Bahan Dasar masakah",
                    style: new TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 20.0,
                    ),
                  ),
                  new Padding(
                      padding: EdgeInsets.only(top: 4.0)
                  ),
                  new Text("Cara Memasak",
                    style: new TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 20.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          //top rating
          new Card(
            margin: EdgeInsets.symmetric(horizontal: 0.0,vertical: 0.0),
            elevation: 1.0,
            child: new Padding(
                padding: EdgeInsets.all(14.0),
                child: new Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      width: 100.0,
                      child: new Text("Beri Rating Resep Ini",
                        style: TextStyle(
                          fontSize: 14.0
                        ),
                        maxLines: 2,
                      ),
                    ),
                    new SmoothStarRating(
                      allowHalfRating: false,
                      starCount: 5,
                      size: 45.0,
                      color: Colors.yellow,
                      borderColor: Colors.yellow,
                      rating: rating,
                      onRatingChanged: (v){
                        rating = v;
                        setState(() {
                        });
                      },
                    ),
                  ],
                ),
            )
          ),
          //spesifikasi resep
          new Card(
            margin: EdgeInsets.only(top: 4.0),
            elevation: 1.0,
            child: new Container(
                height: 120.0,
                child: new FutureBuilder(
                    future: api.getUpcomingMovie(),
                    builder: (context,apiData){
                      switch (apiData.connectionState) {
                        case ConnectionState.none: return new Text('Press button to start');
                        case ConnectionState.waiting:
                          return new Shimmer.fromColors(
                              baseColor: Colors.grey[300],
                              highlightColor: Colors.grey[100],
                              child: new Container(
                                color: Colors.white,
                                height: 120.0,
                              )
                          );
                        default:
                          if (apiData.hasError)
                            return new Text('Error: ${apiData.error}');
                          else{
                            print(apiData.data);
                            List<Movie> data = apiData.data;
                            print(data.length);
                            return new ListView.builder(
                              padding: EdgeInsets.symmetric(vertical: 12.0),
                                scrollDirection: Axis.horizontal,
                                itemCount: data.length,
                                itemBuilder: (context,index)=>new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
//                                  alignment: Alignment.bottomRight,
                                  children: <Widget>[
                                    new Container(
                                      width: 65.0,
                                      height: 65.0,
                                      decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                              fit: BoxFit.fill,
                                              image: new CachedNetworkImageProvider("http://image.tmdb.org/t/p/w185${data[index].posterPath!=null?data[index].posterPath:data[index-1].posterPath}"))
                                      ),
                                      margin: const EdgeInsets.symmetric(horizontal: 8.0),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.only(top: 4.0),
                                      width: 65.0,
                                      child: new Text(data[index].title,
                                        maxLines: 2,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 10.0,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                            );
                          }
                      }
                    }
                ),
            ),
          ),
          //bahan dasar makanan
          new Card(
            margin: EdgeInsets.only(top: 2.0),
            child: new Padding(
              padding: EdgeInsets.all(14.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text("Bahan Dasar Makanan",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                  new ListView.builder(
                      padding: EdgeInsets.only(bottom: 10.0),
                      physics: ClampingScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: 10,
                      itemBuilder: (context,index)=>
                      new ListTile(
                        leading: Text((index+1).toString()),
                        title: Text(
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                          maxLines: 4,
                          style: TextStyle(
                              fontSize: 14.0
                          ),
                        ),
                      )
                  )
                ],
              ),
            )
          ),
          //cara memasak
          new Card(
            margin: EdgeInsets.only(top: 4.0,bottom: 4.0),
            child: new Padding(
                padding: EdgeInsets.all(14.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text("Cara Memasak",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                  new RecipeSwiper(),
                ],
              ),
            ),
          ),
          //bottom rating
          new Card(
              margin: EdgeInsets.symmetric(horizontal: 0.0,vertical: 0.0),
              elevation: 1.0,
              child: new Padding(
                padding: EdgeInsets.all(14.0),
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: new Text("Beri Rating Resep Ini",
                        style: TextStyle(
                            fontSize: 14.0
                        ),
                        maxLines: 1,
                      ),
                    ),
                    new SmoothStarRating(
                      allowHalfRating: false,
                      starCount: 5,
                      size: 45.0,
                      color: Colors.yellow,
                      borderColor: Colors.yellow,
                      rating: rating,
                      onRatingChanged: (v){
                        rating = v;
                        setState(() {
                        });
                      },
                    ),
                  ],
                ),
              )
          ),
          //Tag
          new Card(
            margin: EdgeInsets.only(top: 4.0),
            elevation: 1.0,
            child: new Padding(
              padding: EdgeInsets.all(14.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: new Text("Tag",
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600
                      ),
                    ),
                  ),
                  new Container(
                    height: 100.0,
                    child: new FutureBuilder(
                        future: api.getUpcomingMovie(),
                        builder: (context,apiData){
                          switch (apiData.connectionState) {
                            case ConnectionState.none: return new Text('Press button to start');
                            case ConnectionState.waiting:
                              return new Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[100],
                                  child: new Container(
                                    color: Colors.white,
                                    height: 120.0,
                                  )
                              );
                            default:
                              if (apiData.hasError)
                                return new Text('Error: ${apiData.error}');
                              else{
                                print(apiData.data);
                                List<Movie> data = apiData.data;
                                print(data.length);
                                return new ListView.builder(
                                    padding: EdgeInsets.symmetric(vertical: 12.0),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: data.length,
                                    itemBuilder: (context,index)=> new Container(
                                      width: 65.0,
                                      height: 65.0,
                                      margin: const EdgeInsets.symmetric(horizontal: 8.0),
                                      decoration: new BoxDecoration(
                                        border: new Border.all(color: Colors.grey),
                                        shape: BoxShape.circle,
                                      ),
                                      child: new Text(data[index].popularity.toString()),
                                    )
                                );
                              }
                          }
                        }
                    ),
                  )
                ],
              ),
            ),
          ),
          new ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
              itemCount: 40,
              itemBuilder: (context,index)=>
                  new Text("list $index")
          )
        ],
      )
    );
  }
}
