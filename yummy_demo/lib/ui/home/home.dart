import 'package:flutter/material.dart';
import 'package:yummy_demo/ui/home/body.dart';
import 'package:yummy_demo/features/recipe_swiper.dart';
import 'package:yummy_demo/features/recipe_vertical_grid.dart';
import 'package:yummy_demo/features/recipe_horizontal.dart';
import 'package:yummy_demo/features/recipe_stories.dart';

class Home extends StatefulWidget {

  @override
  HomeState createState() {
    return new HomeState();
  }
}

class HomeState extends State<Home> {

  ScrollController _controller = new ScrollController();
  @override
  void initState() {
    super.initState();
    _controller.addListener((){
//      print(_controller.position.pixels);
    });
  }

  final topBar = AppBar(
    backgroundColor: new Color(0xfff8faf8),
    centerTitle: true,
    elevation: 1.0,
    leading: new Icon(Icons.camera_alt),
    title: new Text("Yummy",style: new TextStyle(fontFamily: "Pacifico"),),
    actions: <Widget>[
      Padding(
        padding: const EdgeInsets.only(right: 12.0),
        child: Icon(Icons.send),
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: topBar,
      body: new CustomScrollView(
        controller: _controller,
        slivers: <Widget>[
          SliverList(
              delegate: SliverChildListDelegate([
                Column(
                  children: <Widget>[
                    new RecipeStories(),
                    new RecipeHorizontal(),
                    new RecipeSwiper(),
                    new RecipeVerticalGrid(_controller),
//                  new RecipeList()
                  ],
                )
              ])
          )
        ],
      ),
      bottomNavigationBar: new Container(
        color: Colors.white,
        height: 50.0,
        child: new BottomAppBar(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new IconButton(icon: Icon(Icons.home), onPressed: null),
              new IconButton(icon: Icon(Icons.search), onPressed: null),
              new IconButton(icon: Icon(Icons.add_box), onPressed: null),
              new IconButton(icon: Icon(Icons.favorite), onPressed: null),
              new IconButton(icon: Icon(Icons.account_box), onPressed: null),
            ],
          ),
        ),
      ),
    );
  }
}
