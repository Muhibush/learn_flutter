import 'package:flutter/material.dart';
import 'package:yummy_demo/features/recipe_swiper.dart';
import 'package:yummy_demo/features/recipe_vertical_grid.dart';
import 'package:yummy_demo/features/recipe_horizontal.dart';
import 'package:yummy_demo/features/recipe_stories.dart';

class InstaBody extends StatefulWidget {
  @override
  _InstaBodyState createState() => _InstaBodyState();
}

class _InstaBodyState extends State<InstaBody> {
  ScrollController _controller = new ScrollController();
  @override
  void initState() {
    super.initState();
    _controller.addListener((){
//      print(_controller.position.pixels);
    });
  }
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _controller,
      slivers: <Widget>[
        SliverList(
            delegate: SliverChildListDelegate([
              Column(
                children: <Widget>[
                  new RecipeStories(),
                  new RecipeHorizontal(),
                  new RecipeSwiper(),
                  new RecipeVerticalGrid(_controller),
//                  new RecipeList()
                ],
              )
            ])
        )
      ],
    );
  }
}

