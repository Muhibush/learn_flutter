class Movie{
  final String title;
  final double popularity;
  final String overview;
  final String posterPath;
  final String backdropPath;

  Movie({this.title, this.popularity, this.overview, this.posterPath, this.backdropPath});

  factory Movie.fromJson(Map json){
    return Movie(
      title: json['title'] as String,
      popularity: json['popularity'] as double,
      overview: json['overview'] as String,
      posterPath: json['poster_path'] as String,
      backdropPath: json['backdrop_path'] as String
    );
  }

}