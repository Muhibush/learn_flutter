import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:yummy_demo/model/movie.dart';

final _apiKey = "616ddd8bfc0d9e590d39999244591782";

Future<List> getUpcomingMovie() async{
  String url = "https://api.themoviedb.org/3/movie/upcoming?api_key=$_apiKey&language=en-US";
//  http.Response response = await http.get(url);
//  Map data = json.decode(response.body);
//  return compute(parsePhotos,data['results'].toString());
  http.Response response = await http.get(url);
  return compute(parseMovie,response.body);
}

List<Movie> parseMovie(String responseBody) {
  List data = json.decode(responseBody)['results'];
  final parsed = data.cast<Map<String, dynamic>>();
  return parsed.map<Movie>((json) => Movie.fromJson(json)).toList();
}