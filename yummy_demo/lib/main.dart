import 'package:flutter/material.dart';
import 'package:yummy_demo/ui/home/home.dart';

void main(){

//  Map data = await api.getUpcomingMovie();
//  print(data.toString());
  runApp(
      new MaterialApp(
        title: 'Yummy',
        home: new Home(),
        theme: new ThemeData(
            primaryColor: Colors.white,
            primaryIconTheme: IconThemeData(color: Colors.black),
            primaryTextTheme: TextTheme(
                title: TextStyle(color: Colors.black, fontFamily: "Aveny")
            ),
            textTheme: new TextTheme(
                title: TextStyle(color: Colors.black, fontFamily: "Aveny"))
        ),
      )
  );
}

