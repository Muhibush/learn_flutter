import 'package:flutter/material.dart';
import 'package:yummy_demo/services/api.dart' as api;
import 'package:yummy_demo/items/card_normal.dart';
import 'package:yummy_demo/placeholder/placeholder_card_normal.dart';

class RecipeHorizontal extends StatefulWidget {
  @override
  _RecipeHorizontalState createState() => _RecipeHorizontalState();
}

class _RecipeHorizontalState extends State<RecipeHorizontal> {
  @override

  final itemHorizontal = new Container(
    height: 240.0,
    child: new FutureBuilder(
        future: api.getUpcomingMovie(),
        builder: (context,apiData){
          switch(apiData.connectionState){
            case ConnectionState.none: return new Text('Press button to start');
            case ConnectionState.waiting:
              return new ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  placeholderCardNormal,
                  placeholderCardNormal,
                  placeholderCardNormal,
                  placeholderCardNormal,
                  placeholderCardNormal,
                  placeholderCardNormal,
                ],
              );
            default:
              if (apiData.hasError)
                return new Text('Error: ${apiData.error}');
              else{
                List data = apiData.data;
                return new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: data.length,
                  itemBuilder: (context,index){
                    return cardNormal(context,data[index]);
                  },
                );
              }
          }
        }
    ),
  );

  Widget build(BuildContext context) {
    return itemHorizontal;
  }
}
