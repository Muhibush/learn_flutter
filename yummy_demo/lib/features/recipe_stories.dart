import 'package:flutter/material.dart';
import 'package:yummy_demo/services/api.dart' as api;
import 'package:yummy_demo/model/movie.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';
import 'package:yummy_demo/placeholder/placeholder_item_stories.dart';


class RecipeStories extends StatefulWidget {
  @override
  _RecipeStoriesState createState() => _RecipeStoriesState();
}

class _RecipeStoriesState extends State<RecipeStories> {

  final topText = Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text("Stories", style: new TextStyle(fontWeight: FontWeight.bold)),
      new Row(
        children: <Widget>[
          new Icon(Icons.play_arrow),
          new Text("Watch All", style: TextStyle(fontWeight: FontWeight.bold),)
        ],
      ),
    ],
  );

  final Stories = new Expanded(
    child: new FutureBuilder(
      future: api.getUpcomingMovie(),
      builder: (context,apiData){
        switch (apiData.connectionState) {
          case ConnectionState.none: return new Text('Press button to start');
          case ConnectionState.waiting:
            return new ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                placeholderItemStories,
                placeholderItemStories,
                placeholderItemStories,
                placeholderItemStories,
                placeholderItemStories,
                placeholderItemStories,
                placeholderItemStories,
              ],
            );
          default:
            if (apiData.hasError)
              return new Text('Error: ${apiData.error}');
            else{
              print(apiData.data);
              List<Movie> data = apiData.data;
              print(data.length);
              return new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: data.length,
                  itemBuilder: (context,index)=>new Stack(
                    alignment: Alignment.bottomRight,
                    children: <Widget>[
                      new Container(
                        width: 60.0,
                        height: 60.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: new CachedNetworkImageProvider("http://image.tmdb.org/t/p/w185${data[index].posterPath!=null?data[index].posterPath:data[index-1].posterPath}"))
                        ),
                        margin: const EdgeInsets.symmetric(horizontal: 8.0),
                      ),
                      index == 0 ? Positioned(
                        right: 10.0,
                        child: CircleAvatar(
                          backgroundColor: Colors.blueAccent,
                          radius: 10.0,
                          child: new Icon(
                            Icons.add,
                            size: 14.0,
                            color: Colors.white ,),
                        ),
                      ):new Container()
                    ],
                  )
              );
            }
        }
      }
    ),
  );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 110.0,
//      height: MediaQuery.of(context).size.height * 0.17,
      child: Container(
        margin: const EdgeInsets.all(16.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            topText,
            Stories
          ],
        ),
      ),
    );
  }
}

