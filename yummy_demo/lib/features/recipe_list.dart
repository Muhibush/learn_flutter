import 'package:flutter/material.dart';
import 'package:yummy_demo/services/api.dart' as api;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:video_player/video_player.dart';
import 'package:yummy_demo/model/movie.dart';

class RecipeList extends StatefulWidget {
  @override
  _RecipeListState createState() => _RecipeListState();
}

class _RecipeListState extends State<RecipeList> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new VideoPlayerController.network(
      'https://github.com/flutter/assets-for-api-docs/blob/master/assets/videos/butterfly.mp4?raw=true',);
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder(
        future: api.getUpcomingMovie(),
        builder: (context,apiData){
          switch (apiData.connectionState) {
            case ConnectionState.none: return new Text('Press button to start');
            case ConnectionState.waiting: return new Text('Awaiting result...');
            default:
              if (apiData.hasError)
                return new Text('Error: ${apiData.error}');
              else{
                List<Movie> data = apiData.data;
                return new ListView.builder(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: data.length,
                    itemBuilder: (context,index){
                      return new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          new Padding(
                            padding: const EdgeInsets.fromLTRB(16.0,16.0,8.0,16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    new Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                              fit: BoxFit.fill,
                                              image: new NetworkImage("http://image.tmdb.org/t/p/w185${data[index].posterPath!= null ?data[index].posterPath:data[index-1].posterPath}"))
                                      ),
                                    ),
                                    new SizedBox(
                                      width: 10.0,
                                    ),
                                    new Text(data[index].title,style: TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                new IconButton(icon: Icon(Icons.more_vert), onPressed: null)
                              ],
                            ),
                          ),
                          new Flexible(
                              child: new Image.network(
                                  "http://image.tmdb.org/t/p/w300${data[index].backdropPath!=null?data[index].backdropPath:data[index-1].backdropPath}",
                                  fit: BoxFit.fill,
                              )
                          ),
//                          new Flexible(
//                              fit: FlexFit.loose,
//                              child: new Chewie(
//                                new VideoPlayerController.network("https://github.com/flutter/assets-for-api-docs/blob/master/assets/videos/butterfly.mp4?raw=true"),
//                                aspectRatio: 3 / 2,
//                                autoPlay: false,
//                                looping: false,
//                                autoInitialize: true,
//                                placeholder: new Image.network("http://image.tmdb.org/t/p/w300${apiData.data['results'][index]['backdrop_path']}"),
//
//                                // Try playing around with some of these other options:
//
//                                // showControls: false,
//                                // materialProgressColors: new ChewieProgressColors(
//                                //   playedColor: Colors.red,
//                                //   handleColor: Colors.blue,
//                                //   backgroundColor: Colors.grey,
//                                //   bufferedColor: Colors.lightGreen,
//                                // ),
//                                // placeholder: new Container(
//                                //   color: Colors.grey,
//                                // ),
//                                // autoInitialize: true,
//                              ),
//                          ),
                          new Padding(
                            padding: const EdgeInsets.all(16.8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Icon(FontAwesomeIcons.heart),
                                    new SizedBox(width: 16.2,),
                                    new Icon(FontAwesomeIcons.comment),
                                    new SizedBox(width: 16.0),
                                    new Icon(FontAwesomeIcons.paperPlane)
                                  ],
                                ),
                                new Icon(FontAwesomeIcons.bookmark)
                              ],
                            ),
                          ),
                          new Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text(
                              "Liked by muhibush, pk and 528,331 others",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          new Padding(
                            padding: const EdgeInsets.fromLTRB(16.0,16.0,0.0,8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  height: 40.0,
                                  width: 40.0,
                                  decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                          fit: BoxFit.fill,
                                          image: new NetworkImage("https://yt3.ggpht.com/-UPQp5s0GXPs/AAAAAAAAAAI/AAAAAAAAAAA/KRiAFGjp6N0/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"))
                                  ),
                                ),
                                new SizedBox(
                                  width: 10.0,
                                ),
                                Expanded(
                                  child: new TextField(
                                    decoration: new InputDecoration(
                                        border: InputBorder.none,
                                        hintText: "Add a comment ..."
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          new Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text("1 Day Ago", style: TextStyle(color: Colors.grey),),
                          )
                        ],
                      );
                    }
                );
              }
          }
        }
    );
  }
}

