import 'package:flutter/material.dart';
import 'package:yummy_demo/services/api.dart' as api;
import 'package:yummy_demo/items/card_normal.dart';
import 'package:yummy_demo/model/movie.dart';

class RecipeVerticalGrid extends StatefulWidget {
  ScrollController controller;

  RecipeVerticalGrid(ScrollController controller){
    this.controller = controller;
  }

  @override
  _RecipeVerticalGridState createState() => _RecipeVerticalGridState(controller);
}

class _RecipeVerticalGridState extends State<RecipeVerticalGrid> {
  List<Movie> data = new List();
  ScrollController _scrollController = new ScrollController();
  ScrollController controller;

  _RecipeVerticalGridState(ScrollController controller){
    this.controller = controller;
  }
  
  @override
  void initState() {
    super.initState();
    fetchData();
    print("initState");

    controller.addListener((){
      print("posisi ${controller.position.pixels}");
      if(controller.position.pixels == controller.position.maxScrollExtent){
        fetchData();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    print("BuildContext");
    print("data length${data.length}");
    return new GridView.count(
      controller: _scrollController,
      shrinkWrap: true,
      padding: new EdgeInsets.symmetric(horizontal: 10.0),
      crossAxisCount: 2,
      childAspectRatio: 0.7,
      children: List.generate(data.length, (index){
        print("data length ${data.length}");
        return cardNormal(context,data[index]);
      }),
    );
  }

  fetchData() async{
    List<Movie> response = await api.getUpcomingMovie();
    setState(() {
      data.addAll(response);
    });
    print("'''''''''''''''''''''''''''''''");
    print("data length fetch ${data.length}");
  }
}
