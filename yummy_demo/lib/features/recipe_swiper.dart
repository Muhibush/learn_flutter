import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:yummy_demo/services/api.dart' as api;
import 'package:yummy_demo/model/movie.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:yummy_demo/placeholder/placeholder_image_swiper.dart';

class RecipeSwiper extends StatefulWidget {
  @override
  _RecipeSwiperState createState() => _RecipeSwiperState();
}

class _RecipeSwiperState extends State<RecipeSwiper> {
  final recipeSwiper = new Container(
      child: new FutureBuilder(
        future: api.getUpcomingMovie(),
        builder: (context,apiData){
          switch(apiData.connectionState){
            case ConnectionState.none: return new Text("none");
            case ConnectionState.waiting:
              return new Swiper(
                layout: SwiperLayout.TINDER,
                itemCount: 3,
                itemWidth: 300.0,
                itemHeight: 400.0,
                itemBuilder: (context,index){
                  return placeholderImageSwiper;
                },
              );
            default:
              if (apiData.hasError) return new Text("Error ${apiData.error}");
              else{
                List<Movie> data = apiData.data;
                return new Swiper(
                  layout: SwiperLayout.TINDER,
                  itemCount: data.length,
                  itemWidth: 300.0,
                  itemHeight: 400.0,
                  itemBuilder: (context,index){
                    return CachedNetworkImage(
                      imageUrl: "http://image.tmdb.org/t/p/w300${data[index].posterPath!=null?data[index].posterPath:"/VuukZLgaCrho2Ar8Scl9HtV3yD.jpg"}",
                      fit: BoxFit.fill,
                      height: 130.0,
                      width: 100.0,
                      placeholder: placeholderImageSwiper,
                    );
                  },
                );
              }
          }
        },
      )
  );

  @override
  Widget build(BuildContext context) {
    return recipeSwiper;
  }
}
