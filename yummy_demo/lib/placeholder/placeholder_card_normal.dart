import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget placeholderCardNormal = new Shimmer.fromColors(
  baseColor: Colors.grey[300],
  highlightColor: Colors.grey[100],
  child: new Container(
    color: Colors.white,
    width: 200.0,
    height: 200.0,
    margin: EdgeInsets.symmetric(horizontal: 5.0),
  ),
);