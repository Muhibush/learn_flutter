import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget placeholderItemStories = new Shimmer.fromColors(
  baseColor: Colors.grey[300],
  highlightColor: Colors.grey[100],
  child: new Container(
    width: 60.0,
    height: 60.0,
    margin: const EdgeInsets.symmetric(horizontal: 8.0),
    decoration: new BoxDecoration(
        color: Colors.red,
        shape: BoxShape.circle
    ),
  ),
);