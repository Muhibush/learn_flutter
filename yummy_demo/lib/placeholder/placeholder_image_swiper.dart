import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget placeholderImageSwiper = new Shimmer.fromColors(
  baseColor: Colors.grey[300],
  highlightColor: Colors.grey[100],
  child: new Container(
    color: Colors.white,
    height: 130.0,
    width: 100.0,
  ),
);