import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() async {
  List _data = await getJson();
  print(_data[8]);
  runApp(
      new MaterialApp(
        home: new Scaffold(
          appBar: new AppBar(
            title: new Text("JSON PArse"),
            centerTitle: true,
          ),
          body: new Center(
            child: new ListView.builder(
                itemCount: _data.length,
                padding: const EdgeInsets.all(16.0),
                itemBuilder: (contex,index){
                  return new ListTile(
                    leading: new CircleAvatar(
                      backgroundColor: Colors.green,
                      child: new Text("${_data[index]['title'][0].toString().toUpperCase()}",
                        style: new TextStyle(
                          fontSize: 13.4,
                          color: Colors.white
                        ),
                      ),
                    ),
                    title: new Text("${_data[index]['id']}",
                      style: new TextStyle(fontSize: 18.0),
                    ),
                    subtitle: new Divider(),
                  );
                }
            ),
          ),
        ),
      )
  );
}

Future<List> getJson() async{
  String apiUrl = 'https://jsonplaceholder.typicode.com/posts';

  http.Response response = await http.get(apiUrl);

  return json.decode(response.body);
}