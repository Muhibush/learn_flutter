import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

final _apiKey = "616ddd8bfc0d9e590d39999244591782";

Future<Map> getUpcomingMovie() async{
  String url = "https://api.themoviedb.org/3/movie/upcoming?api_key=$_apiKey&language=en-US";
  http.Response response = await http.get(url);
  return json.decode(response.body);
}