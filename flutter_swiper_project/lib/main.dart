import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_swiper_project/api.dart' as api;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePageApi(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Swiper(
        itemBuilder: (context,index){
          return new Image.network(
              "http://via.placeholder.com/288x188",
              fit: BoxFit.fill,
          );
        },
        itemCount: 10,
        itemWidth: 300.0,
        itemHeight: 400.0,
      layout: SwiperLayout.TINDER,
    );
  }
}

class MyHomePageApi extends StatefulWidget {
  @override
  _MyHomePageApiState createState() => _MyHomePageApiState();
}

class _MyHomePageApiState extends State<MyHomePageApi> {
  @override
  Widget build(BuildContext context) {
    return new FutureBuilder(
        future: api.getUpcomingMovie(),
        builder: (context,apiData){
          switch(apiData.connectionState){
            case ConnectionState.none: return new Text("none");
            case ConnectionState.waiting: return new Text("waiting");
            default:
              if (apiData.hasError) return new Text("Error ${apiData.error}");
              else{
                List data = apiData.data['results'];
                return new Swiper(
                  layout: SwiperLayout.TINDER,
                  itemCount: data.length,
                  itemWidth: 300.0,
                  itemHeight: 400.0,
                  itemBuilder: (context,index){
                    return new Image.network(
                        "http://image.tmdb.org/t/p/w185${apiData.data['results'][index]['poster_path']}",
                        fit: BoxFit.fill,
                    );
                  },
                );
              }
          }
        },
    );
  }
}



