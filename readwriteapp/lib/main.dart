import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart'; //https://pub.dartlang.org/packages/path_provider#-installing-tab-

void main() async {
  var data = await readData();

  if (data != null) {
    String message = await readData();
    print(message);
  }

  runApp(new MaterialApp(
    title: 'IO',
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  var _enterDataField = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Read/Write'),
        centerTitle: true,
        backgroundColor: Colors.greenAccent,
      ),
      body: new Container(
        height: 1200.0,
        padding: const EdgeInsets.all(13.4),
        child: new Column(
          children: <Widget>[
            new TextField(
              controller: _enterDataField,
              decoration: new InputDecoration(labelText: 'Write Something'),
            ),
            new FlatButton(
                onPressed: (){
                  writeData(_enterDataField.text);
                },
                child: new Text("Save data")
            ),
//            new Text("No data saved")
            Container(
              color: Colors.redAccent,
              padding: const EdgeInsets.all(13.4),
              child: new FutureBuilder(
                  future: readData(),
                  builder:
                      (context,data) {
                    if (data.hasData != null) {
                      print(data.toString());
                      return new Text(data.data.toString());
                    } else {
                      return new Text("No data saved");
                    }
                  }
              ),
            )
          ],
        ),
      ),
    );
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path; //home/directory/
}

Future<File> get _localFile async {
  final path = await _localPath;

  return new File('$path/data.txt'); //home/directory/data.txt
}

//Write and Read from our file
Future<File> writeData(String message) async {
  final file = await _localFile;

  //write to file
  return file.writeAsString('$message');
}

Future<String> readData() async {
  try {
    final file = await _localFile;

    //Read
    String data = await file.readAsString();

    return data;
  } catch (e) {
    return "Nothing saved yet!";
  }
}
