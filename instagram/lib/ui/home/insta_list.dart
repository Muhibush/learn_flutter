import 'package:flutter/material.dart';
import 'package:instagram/ui/home/insta_stories.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InstaList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: 5,
        itemBuilder: (context,index) => index == 0
        ? new SizedBox(
          child: new InstaStories(),
          height: MediaQuery.of(context).size.height * 0.17,
        ):
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Padding(
              padding: const EdgeInsets.fromLTRB(16.0,16.0,8.0,16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      new Container(
                        height: 40.0,
                        width: 40.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: new NetworkImage("https://yt3.ggpht.com/-UPQp5s0GXPs/AAAAAAAAAAI/AAAAAAAAAAA/KRiAFGjp6N0/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"))
                        ),
                      ),
                      new SizedBox(
                        width: 10.0,
                      ),
                      new Text("Muhibush",style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  new IconButton(icon: Icon(Icons.more_vert), onPressed: null)
                ],
              ),
            ),
            new Flexible(
                fit: FlexFit.loose,
                child: new Image.network("https://cdn.images.express.co.uk/img/dynamic/143/590x/PUBG-server-down-992798.jpg?r=1532332988974",
                    fit: BoxFit.cover,)
            ),
            new Padding(
                padding: const EdgeInsets.all(16.8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Icon(FontAwesomeIcons.heart),
                        new SizedBox(width: 16.2,),
                        new Icon(FontAwesomeIcons.comment),
                        new SizedBox(width: 16.0),
                        new Icon(FontAwesomeIcons.paperPlane)
                      ],
                    ),
                    new Icon(FontAwesomeIcons.bookmark)
                  ],
                ),
            ),
            new Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                "Liked by muhibush, pk and 528,331 others",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            new Padding(
                padding: const EdgeInsets.fromLTRB(16.0,16.0,0.0,8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage("https://yt3.ggpht.com/-UPQp5s0GXPs/AAAAAAAAAAI/AAAAAAAAAAA/KRiAFGjp6N0/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"))
                      ),
                    ),
                    new SizedBox(
                      width: 10.0,
                    ),
                    Expanded(
                      child: new TextField(
                        decoration: new InputDecoration(
                          border: InputBorder.none,
                          hintText: "Add a comment ..."
                        ),
                      ),
                    )
                  ],
                ),
            ),
            new Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Text("1 Day Ago", style: TextStyle(color: Colors.grey),),
            )
          ],
        )
    );
  }
}
