import 'package:flutter/material.dart';
import 'package:instagram/ui/home/home.dart';

void main() => runApp(
    new MaterialApp(
      title: 'Instagram',
      home: new Home(),
      theme: new ThemeData(
        primaryColor: Colors.black,
        primaryIconTheme: IconThemeData(color: Colors.black),
        primaryTextTheme: TextTheme(
            title: TextStyle(color: Colors.black,fontFamily: "Aveny")
        ),
        textTheme: new TextTheme(title: TextStyle(color: Colors.black))
      ),
    )
);

