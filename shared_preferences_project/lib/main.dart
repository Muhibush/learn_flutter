import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(new MaterialApp(
    title: "Shared Preferences",
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _teSharedController = new TextEditingController();
  String _text = "";

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  _loadData() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      if (preferences.getString('data') != null && preferences.getString('data').isNotEmpty ) {
        _text = preferences.getString("data");
      }else {
        _text= "Empty SP";

      }
    });
  }

  _saveData(String value) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('data', value);
    
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("SharedPref"),
        centerTitle: true,
        backgroundColor: Colors.deepOrange,
      ),
      body: new Column(
        children: <Widget>[
          new TextField(
            controller: _teSharedController,
            decoration: new InputDecoration(labelText: 'Write something'),
          ),
          new FlatButton(
              onPressed: (){
                _saveData(_teSharedController.text);
              },
              child: new Text("Save data")
          ),
          Container(
            color: Colors.green,
            child:new Text(_text,style: new TextStyle(color: Colors.red),),
          )
        ],
      ),
    );
  }
}


