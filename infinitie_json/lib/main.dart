import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _total = 3;

  @override
  Widget build(BuildContext context) {
    var listView = new ListView.builder(

      itemCount: _total,
        itemBuilder: (context,index){
          return new ListTile(
            title: new Text("item"),

          );
        }
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Future Infinitie List View"),
      ),
      body: listView ,
    );
  }

  Future<String> _getJson(int offset,int limit) async{
    String json = "";
    return json;
  }
}
