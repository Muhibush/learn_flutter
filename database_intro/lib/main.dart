import 'package:database_intro/models/user.dart';
import 'package:flutter/material.dart';
import 'package:database_intro/utils/database_helper.dart';

List _users;
void main()async{

  var db = new DatabaseHelper();

  // create a User
//  int saveduser = await db.saveUser(new User("Monk","rina"));
//  print("User saved $saveduser");

  //Read a User
//  User ana = await db.getUser(1);
//  print("Got username: ${ana.username}");
//  int count = await db.getCount();
//  print("Count: $count");

  //Update a User
//  User userUpdated = User.fromMap({
//    "username"  : "updatedUsername",
//    "password"  : "UpdatedPassword",
//    "id"        : 1
//  });
//  await db.updateUser(userUpdated);

  //Delete a User
//  int userDelete = await db.deleteUser(2);
//  print("Delete User: $userDelete");


  _users = await db.getAllUsers();
  for(int i=0;i<_users.length;i++){
    User user = User.map(_users[i]);
    print("Username: ${user.username}, id: ${user.id}");
  }

  runApp(new MaterialApp(
    title: "DatabaseSqlite",
    home: new Home(),
  ));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("DatabaseSqlite"),
        centerTitle: true,
        backgroundColor: Colors.lightGreen,
      ),
      body: new ListView.builder(
          itemCount: _users.length,
          itemBuilder: (context,index){
            return new Card(
              color: Colors.white,
              elevation: 2.0,
              child: new ListTile(
                title: new Text("User: ${User.fromMap(_users[index]).username}"),
                subtitle: new Text("Id: ${User.fromMap(_users[index]).id}"),
                onTap: (){
                  debugPrint("User: ${User.fromMap(_users[index]).username}");
                },
              ),
            );
          }
      ),
    );
  }
}

